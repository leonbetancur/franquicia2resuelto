package co.com.franquicia2resuelto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Franquicia2resueltoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Franquicia2resueltoApplication.class, args);
	}

}
