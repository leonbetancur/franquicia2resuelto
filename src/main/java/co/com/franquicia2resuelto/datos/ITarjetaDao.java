package co.com.franquicia2resuelto.datos;

import org.springframework.stereotype.Repository;

import co.com.franquicia2resuelto.datos.entidades.TarjetaCursoJava;

@Repository
public interface ITarjetaDao {
	
	public void adicionarTarjeta(TarjetaCursoJava tarjetaCursoJava);
	
	public void actualizarTarjeta(TarjetaCursoJava tarjetaCursoJava);

	public TarjetaCursoJava tarjetaPorNumero(String numero);
}
