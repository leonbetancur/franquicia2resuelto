package co.com.franquicia2resuelto.datos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import co.com.franquicia2resuelto.datos.entidades.TarjetaCursoJava;

@Repository
public class TarjetaDaoImpl implements ITarjetaDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private static final String INSERT = "INSERT INTO TARJETA_CURSO_JAVA"
										+ "(numero, usuario, saldo, deuda, nombre, vencimiento, codigo_seguridad, tipo_documento, documento)"
										+"	VALUES(:numero, :usuario, :saldo, :deuda, :nombre, :vencimiento, :codigo_seguridad, :tipo_documento, :documento)";
	
	private static final String SELECT = "SELECT numero, usuario, saldo, deuda, nombre, vencimiento, codigo_seguridad, tipo_documento, documento"
										+ "	FROM TARJETA_CURSO_JAVA"
										+ " WHERE numero =:numero";
	
	@Override
	public void adicionarTarjeta(TarjetaCursoJava tarjetaCursoJava) {
		Map<String,Object> parametros = new HashMap<>();
		parametros.put("numero", tarjetaCursoJava.getNumero());
		parametros.put("usuario", tarjetaCursoJava.getUsuario());
		parametros.put("saldo", tarjetaCursoJava.getSaldo());
		parametros.put("deuda", tarjetaCursoJava.getDeuda());
		parametros.put("nombre", tarjetaCursoJava.getNombre());
		parametros.put("vencimiento", tarjetaCursoJava.getVencimiento());
		parametros.put("codigo_seguridad", tarjetaCursoJava.getCodigoSeguridad());
		parametros.put("tipo_documento", tarjetaCursoJava.getTipoDocumento());
		parametros.put("documento", tarjetaCursoJava.getDocumento());

		jdbcTemplate.update(INSERT, parametros);
	}

	@Override
	public void actualizarTarjeta(TarjetaCursoJava tarjetaCursoJava) {
		// TODO Auto-generated method stub

	}

	@Override
	public TarjetaCursoJava tarjetaPorNumero(String numero) {
		Map<String,Object> parametro = new HashMap<>();
		parametro.put("numero", numero);
		return jdbcTemplate.queryForObject(SELECT, parametro, ( rs, row) -> registroTarjeta(rs));
		
	}

	private TarjetaCursoJava registroTarjeta(ResultSet rs) throws SQLException {
		TarjetaCursoJava tarjeta = new TarjetaCursoJava();
		tarjeta.setNumero(rs.getString("numero"));
		tarjeta.setUsuario(rs.getString("usuario"));
		tarjeta.setSaldo(rs.getBigDecimal("saldo"));
		tarjeta.setDeuda(rs.getBigDecimal("deuda"));
		tarjeta.setNombre(rs.getString("nombre"));
		tarjeta.setVencimiento(rs.getString("vencimiento"));
		tarjeta.setCodigoSeguridad(rs.getString("codigo_seguridad"));
		tarjeta.setTipoDocumento(rs.getString("tipo_documento"));
		tarjeta.setDocumento(rs.getString("documento"));
		return tarjeta;
	}
}
