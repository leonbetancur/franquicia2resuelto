package co.com.franquicia2resuelto.dto;


public class Tarjeta {

	private String nombreTarjeta;
	private String numero;
	private String vencimiento;
	private String codigoSeguridad;
	
	public Tarjeta() {}
	public Tarjeta(String nombreTarjeta,
					String numero,
					String vencimiento,
					String codigoSeguridad) {
		this.nombreTarjeta = nombreTarjeta;
		this.numero = numero;
		this.vencimiento = vencimiento;
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getNombreTarjeta() {
		return nombreTarjeta;
	}

	public String getNumero() {
		return numero;
	}

	public String getVencimiento() {
		return vencimiento;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoSeguridad == null) ? 0 : codigoSeguridad.hashCode());
		result = prime * result + ((nombreTarjeta == null) ? 0 : nombreTarjeta.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((vencimiento == null) ? 0 : vencimiento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarjeta other = (Tarjeta) obj;
		if (codigoSeguridad == null) {
			if (other.codigoSeguridad != null)
				return false;
		} else if (!codigoSeguridad.equals(other.codigoSeguridad))
			return false;
		if (nombreTarjeta == null) {
			if (other.nombreTarjeta != null)
				return false;
		} else if (!nombreTarjeta.equals(other.nombreTarjeta))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (vencimiento == null) {
			if (other.vencimiento != null)
				return false;
		} else if (!vencimiento.equals(other.vencimiento))
			return false;
		return true;
	}
	
	
					
}
