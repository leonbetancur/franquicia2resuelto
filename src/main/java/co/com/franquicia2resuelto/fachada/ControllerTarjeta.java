package co.com.franquicia2resuelto.fachada;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.franquicia2resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia2resuelto.dto.Tarjeta;
import co.com.franquicia2resuelto.servicios.ServicioTarjetas;

@RestController
@RequestMapping("franquicia2")
public class ControllerTarjeta {
	
	@Autowired
	private ServicioTarjetas servicio;
	
	@PostMapping("/tarjeta")
	public Tarjeta generarTarjeta(@RequestBody DatosCreacionTarjeta datosCreacionTarjeta) {
		return servicio.generarTarjeta(datosCreacionTarjeta);
	}
	
	@GetMapping("/tarjeta")
	public Tarjeta consultarTarjeta(String numero) {
		return servicio.consultarTarjeta(numero);
	}
}
