package co.com.franquicia2resuelto.servicios;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.franquicia2resuelto.datos.ITarjetaDao;
import co.com.franquicia2resuelto.datos.entidades.TarjetaCursoJava;
import co.com.franquicia2resuelto.dto.Compra;
import co.com.franquicia2resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia2resuelto.dto.Tarjeta;

@Service
public class ServicioTarjetas {
	
	private GeneradorTarjetas generadorTarjetas;
	private ITarjetaDao tarjetaDao;
	
	private static final int DEUDA_CERO = 0;
	private static final String USUARIO = "LEON";
	
	@Autowired
	public ServicioTarjetas(GeneradorTarjetas generadorTarjetas, ITarjetaDao tarjetaDao) {
		this.generadorTarjetas = generadorTarjetas;
		this.tarjetaDao = tarjetaDao;
	}
	
	@Transactional
	public Tarjeta generarTarjeta(DatosCreacionTarjeta datosCreacionTarjeta) {
		Tarjeta tarjetaGenerada = generadorTarjetas.generarTarjeta(datosCreacionTarjeta);
		tarjetaDao.adicionarTarjeta(tarjetaAEntidad(datosCreacionTarjeta,tarjetaGenerada));
		return tarjetaGenerada;
	}

	public boolean registrarCompra(Compra compra) {
		TarjetaCursoJava tarjetaCursoJava= tarjetaDao.tarjetaPorNumero(compra.getTarjeta().getNumero());
		Tarjeta tarjetaBd = entidadATarjeta(tarjetaCursoJava);
		if(compra.getTarjeta().equals(tarjetaBd)) {
			tarjetaCursoJava.setDeuda(tarjetaCursoJava.getDeuda().subtract(new BigDecimal(compra.getValor())));
			tarjetaDao.actualizarTarjeta(tarjetaCursoJava);
			return true;
		}else {
			return false;
		}
		
	}
	
	public Tarjeta consultarTarjeta(String numero) {
		return entidadATarjeta(tarjetaDao.tarjetaPorNumero(numero));
	}
	
	private TarjetaCursoJava tarjetaAEntidad(DatosCreacionTarjeta datosCreacionTarjeta, Tarjeta tarjetaGenerada) {
		TarjetaCursoJava tarjetaCursoJava = new TarjetaCursoJava();
		tarjetaCursoJava.setCodigoSeguridad(tarjetaGenerada.getCodigoSeguridad());
		tarjetaCursoJava.setDeuda(new BigDecimal(DEUDA_CERO));
		tarjetaCursoJava.setDocumento(datosCreacionTarjeta.getCliente().getId());
		tarjetaCursoJava.setNombre(tarjetaGenerada.getNombreTarjeta());
		tarjetaCursoJava.setNumero(tarjetaGenerada.getNumero());
		tarjetaCursoJava.setSaldo(new BigDecimal(datosCreacionTarjeta.getCupo()));
		tarjetaCursoJava.setTipoDocumento(datosCreacionTarjeta.getCliente().getTipoId());
		tarjetaCursoJava.setUsuario(USUARIO);
		tarjetaCursoJava.setVencimiento(tarjetaGenerada.getVencimiento());
		return tarjetaCursoJava;
	}
	
	private Tarjeta entidadATarjeta(TarjetaCursoJava tarjetaCursoJava) {
		return new Tarjeta(tarjetaCursoJava.getNombre(), tarjetaCursoJava.getNumero(), tarjetaCursoJava.getVencimiento(), tarjetaCursoJava.getCodigoSeguridad());
	}
}
